require "rails_helper"

RSpec.describe Author, type: :model do
  it "must have a name" do
    is_expected.not_to be_valid
  end

  it "can have many mangas" do
    author = Author.create "fujio"
    author.mangas.create(name: "naruto", summary: "lorem ip sum")
    author.mangas.create(name: "bleach", summary: "lorem ip sum")
    expect(author.mangas.count).to eq(2)
  end
end
