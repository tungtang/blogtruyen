require "rails_helper"

RSpec.describe Comment, type: :model do
  it "must include an email" do
    c = Comment.new(email: nil, detail: "lorem ip sum")
    expect(c).not_to be_valid
  end

  it "must have a content" do
    c = Comment.new(email: "nobi@gmail.com", detail: nil)
    expect(c).not_to be_valid
  end
end
