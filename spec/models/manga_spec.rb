require "rails_helper"

RSpec.describe Manga, type: :model do
  it "must have a name" do
    m = Manga.new(name: nil, summary: "lorem ip sum")
    expect(m).not_to be_valid
  end
end
