class Comment < ApplicationRecord
  belongs_to :mangas

  validates :email, presence: true
  validates :detail, presence: true
end
