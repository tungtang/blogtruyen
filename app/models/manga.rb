class Manga < ApplicationRecord
  belongs_to :author
  belongs_to :category
  has_many :comments, dependent: :destroy
  paginates_per 2

  validates :name, uniqueness: true, presence: true
end
