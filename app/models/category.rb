class Category < ApplicationRecord
  has_many :mangas, dependent: :destroy

  validates :name, presence: :true,  uniqueness: true
end
