class CategoriesController < ApplicationController
  def index
    @category = Category.all
  end

  def new
  end

  def create
    @category = Category.new(category_params)
    @category.save
    redirect_to categories_path
  end
  
  def destroy
    @category = Category.find(params[:id])
    @category.destroy
 
    redirect_to categories_path
  end

  def category_params
    params.require(:category).permit(:name)
  end
end


