class MangaController < ApplicationController
  def index
  end

  def read
    @manga = Manga.all.page(params[:page]).per(1)
  end
end
