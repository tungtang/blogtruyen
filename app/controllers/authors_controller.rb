class AuthorsController < ApplicationController
  def index
    @author = Author.all.page(params[:page]).per(4)
  end

  def new
  end

  def create
    @author = Author.new(author_params)
    @author.save
    redirect_to authors_path
  end
  
  def destroy
    @author = Author.find(params[:id])
    @author.destroy
 
    redirect_to authors_path
  end

  def author_params
    params.require(:author).permit(:name)
  end
end
