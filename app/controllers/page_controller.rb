class PageController < ApplicationController
  def homepage
    @mangas = Manga.all.page(params[:page])
  end

end
