Rails.application.routes.draw do
  get 'page/homepage'
  get 'manga/index'
  get 'manga/read'

  resources :categories
  resources :authors

  root 'page#homepage'
end
