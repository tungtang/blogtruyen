class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.string :email
      t.text :detail
      t.belongs_to :manga, foreign_key: true
    end
  end
end
