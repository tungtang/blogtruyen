class CreateMangas < ActiveRecord::Migration[5.2]
  def change
    create_table :mangas do |t|
      t.string :name
      t.text :summary
      t.string :img_path
      t.belongs_to :author, foreign_key: true
      t.belongs_to :category, foreign_key: true
    end
  end
end
