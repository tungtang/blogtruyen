# ruby encoding: utf-8

%w(adventure comedy detective fantasy horror).each do |category|
  Category.create!(name: category)
end

Author.create!(name: "Jeon Kuek Jin")
Author.create!(name: "Sorachi Hideaki")
Author.create!(name: "Fujiko F Fujuo")
Author.create!(name: "Yoshito Usui")
Author.create!(name: "Gosho Aoyama")
Author.create!(name: "Sakuraba Kazuki")
Author.create!(name: "Mashima Hiro")
Author.create!(name: "Kishimoto Masashi")
Author.create!(name: "Eiichiro Oda")
Author.create!(name: "Isayama Hajime")
Author.create!(name: "Takeshi Obata")
Author.create!(name: "Honda Shingo")

Manga.create!(
  name: "Gintama", 
  summary: "Sakata Gintoki is a samurai living in an era when samurai are no longer needed. To add to his troubles, oppressive aliens have moved in to invade. Gintoki lives with Kagura and Shinpachi, taking on odd jobs to make the world a better place... and to pay their rent.",
  img_path: "gin.jpg",
  author_id: Author.find(2).id,
  category_id: Category.find(1).id,
)

Manga.create!(
  name: "Ruler of the Land",
  summary: "Bi-Kwang is a handsome young warrior who becomes a drooling mess whenever he sees a pretty girl. Out on the road he meets an extraordinary swordfighter with no name who is searching for a legendary master warrior. Bi-Kwang promises to aid the swordfighter if he lets Bi-Kwang meet his beautiful sister.",
  img_path: "hiep_khach.jpeg",
  author_id: Author.find(1).id,
  category_id: Category.find(1).id,
)

Manga.create!(
  name: "Doraemon",
  summary: "Doraemon, a cat shaped robot which came from the 22nd century in the future, goes back in time in order to help Nobita, a below average lazy kid, to make his life less miserable and improve his descendent's life. With many of Doreamon's gadgets from the future, Nobita's life will never be as the same.",
  img_path: "doraemon.jpg",
  author_id: Author.find(3).id,
  category_id: Category.find(2).id,
)

Manga.create!(
  name: "Crayon Shin Chan",
  summary: "Crayon Shin-chan (Kureyon Shinchan) is a manga and anime series written by Yoshito Usui. The American version of the manga is titled 'Crayon ShinChan' while the UK, Catalan, Galician, Spanish, Portuguese, Dutch, German, Hindi, Danish and American version of the anime is titled Shin-Chan. The series follows the antics of a five year-old boy Shinnosuke Nohara, his parents, neighbours, and friends.",
  img_path: "shin.jpeg",
  author_id: Author.find(4).id,
  category_id: Category.find(2).id,
)

Manga.create!(
  name: "Detective Conan",
  summary: "Detective Conan Manga follows the experiences of Shinichi Kudo (also called Jimmy Kudo in Case Closed), a young detective prodigy who had been accidentally shrunk right into a child's body as a result of toxin he was force fed by members of a criminal syndicate.",
  img_path: "conan.jpeg",
  author_id: Author.find(5).id,
  category_id: Category.find(3).id,
)

Manga.create!(
  name: "Gosick",
  summary: "Gosick takes place in 1924 in a small, made-up European country of Sauville. The story centers on Kazuya Kujo, the third son of a Japanese Imperial soldier, who is a transfer student to St. Marguerite Academy, where urban legends and horror stories are all the rage.",
  img_path: "gosick.jpg",
  author_id: Author.find(6).id,
  category_id: Category.find(3).id,
)

Manga.create!(
  name: "Fairy Tail",
  summary: "Celestial wizard Lucy wants to join the Fairy Tail, a guild for the most powerful wizards. But instead, her ambitions land her in the clutches of a gang of unsavory pirates led by a devious magician. Her only hope is Natsu, a strange boy she happens to meet on her travels. ",
  img_path: "fairy_tail.jpg",
  author_id: Author.find(7).id,
  category_id: Category.find(4).id,
)

Manga.create!(
  name: "Naruto",
  summary: "Naruto is a manga series from Japan. It's about the story of a young ninja who wants to become the strongest leader in his village. The series were produced by Masashi Kishimoto and in 1997 were published. Later this manga was adapted into a TV anime. ",
  img_path: "naruto.jpeg",
  author_id: Author.find(8).id,
  category_id: Category.find(4).id,
)

Manga.create!(
  name: "One Piece",
  summary: "One Piece follows the adventurous and funny story of Monkey D. Luffy. As a boy, Luffy has always wanted to be the Pirate King. His body obtained the properties of rubber after eating a Devil Fruit. Together with a diverse crew of wannabe pirates, Luffy sets out on the ocean in an attempt to find the world’s ultimate treasure, One Piece.",
  img_path: "one_piece.jpeg",
  author_id: Author.find(9).id,
  category_id: Category.find(4).id,
)

Manga.create!(
  name: "Attack on Titan",
  summary: "Attack on Titan Manga is a series created by Hajime Isayama. The series is based on a fictional story of humanity’s last stand against man-eating giant creatures known as Titans. The series commenced in 2009 and has been going on for 6 years now.",
  img_path: "aot.jpg",
  author_id: Author.find(10).id,
  category_id: Category.find(5).id,
)

Manga.create!(
  name: "Death Note",
  summary: "Death Note Manga is a Japanese manga series written by Tsugumi Ohba and illustrated by Takeshi Obata. The storyline follows a high school student who falls upon a supernatural laptop from a shinigami named Ryuk that allows its user the power to kill anyone whose name and face he understands, Light Yagami.",
  img_path: "death_note.jpeg",
  author_id: Author.find(11).id,
  category_id: Category.find(5).id,
)

Manga.create!(
  name: "Hakaijuu",
  summary: "You lead quite a nice life... Competing with your friends... Having a childhood-almost-girlfriend, who is coming back to town... All dreams starting to come true... when the ground suddenly shakes and a shelf drops on your head.... Well... You hit your head just now, so it'd be quite... reasonable that everything went nuts and people get eaten before your eyes, wouldn't it?",
  img_path: "hakaijuu.jpeg",
  author_id: Author.find(12).id,
  category_id: Category.find(5).id,
)